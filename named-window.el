;;; named-window.el --- Named window save and restore

;; Author: nerding_it <virtualxi99@gmail.com>
;; Created: 14 Jul 2021
;; Keywords: tools
;; Package-Requires: ((emacs "25.1"))
;; Package-Version: 0.0.1
;; Homepage: https://gitlab.com/nerding_it/emacs-named-window

;;; Commentary: 

;; This is simple extension to store window configuration in user friendly names

;;; Code:

(defcustom named-window-size 10
  "Default size to use for storing window configuration."
  :group 'tools
  :type 'number)

(defvar named-window-list (make-ring named-window-size)
  "Ring to hold window configuration.")

(defun named-window-save ()
  "Save window configuration to a name."
  (interactive)
  (let ((name (read-string "Name: ")))
    (ring-insert named-window-list `(,name . ,(current-window-configuration)))))

(defun named-window-restore (window)
  "Restore the named window configuration specified by `WINDOW'."
  (interactive
   (list (ido-completing-read
	 "Name: "
	 (seq-map
	  (lambda (ele) (car ele))
	  (ring-elements named-window-list)))))
  (set-window-configuration (cdr (car (seq-filter
   (lambda (ele) (string-equal (car ele) window))
   (ring-elements named-window-list))))))

(provide 'named-window)

;;; named-window.el ends here
